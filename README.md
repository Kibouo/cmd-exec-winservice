# Cmd Exec WinService
A Windows Service executable taking an arbitrary CLI command. It simply runs it and then stops the service.
Use case: service path hijacking where the environment happens the be stubbornly picky about the executable having to be a service executable.

## Usage
1. change params in `src/main.rs`
    - `SERVICE_NAME` to match the name of the hijacked service
    - `CMD` and `ARGS` to your command
2. build: `cargo build --release`
3. change path of service: `cmd /c sc config <service_name> binPath= "<compiled_exe>"`
4. start service: `cmd /c sc start <service_name>`