use const_format::formatcp;
use std::{
    ffi::OsString,
    fs::OpenOptions,
    io::{Error, ErrorKind, Write},
    process::Command,
    thread::sleep,
    time::Duration,
};
use windows_service::{
    define_windows_service,
    service::{
        ServiceControl, ServiceControlAccept, ServiceExitCode, ServiceState, ServiceStatus,
        ServiceType,
    },
    service_control_handler::{self, ServiceControlHandlerResult},
    service_dispatcher, Error as WindowsServiceError,
};

define_windows_service!(ffi_service_main, rust_service_main);

// No GUI apps supported
const CMD: &str = "curl.exe";
const ARGS: &str = "127.0.0.1:8080";
const SERVICE_NAME: &str = "CmdExecService";

const LOG_FILE: &str = formatcp!("C:\\ProgramData\\{}.log", SERVICE_NAME);

fn main() -> Result<(), WindowsServiceError> {
    // Register generated `ffi_service_main` with the system and start the service, blocking
    // this thread until the service is stopped.
    service_dispatcher::start(SERVICE_NAME, ffi_service_main)?;
    Ok(())
}

fn rust_service_main(arguments: Vec<OsString>) {
    // The entry point where execution will start on a background thread after a call to
    // `service_dispatcher::start` from `main`.
    if let Err(e) = run_service(arguments) {
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .open(LOG_FILE)
            .unwrap();

        let _ = write!(file, "{:?}\r\n", e);
    }
}

fn run_service(_arguments: Vec<OsString>) -> Result<(), WindowsServiceError> {
    let mut service_result = Ok(());

    let event_handler = move |control_event| -> ServiceControlHandlerResult {
        match control_event {
            ServiceControl::Stop => {
                // Handle stop event and return control back to the system.
                ServiceControlHandlerResult::NoError
            }
            // All services must accept Interrogate even if it's a no-op.
            ServiceControl::Interrogate => ServiceControlHandlerResult::NoError,
            _ => ServiceControlHandlerResult::NotImplemented,
        }
    };

    // Register system service event handler, used to report status to system.
    let status_handle = service_control_handler::register(SERVICE_NAME, event_handler)?;

    // Report service as running
    status_handle.set_service_status(ServiceStatus {
        service_type: ServiceType::OWN_PROCESS,
        current_state: ServiceState::Running,
        controls_accepted: ServiceControlAccept::STOP,
        exit_code: ServiceExitCode::Win32(0), // ignored in favor of `ServiceControlHandlerResult`
        checkpoint: 0,
        wait_hint: Duration::default(),
        process_id: None,
    })?;

    let cmd_result = Command::new(CMD).arg(ARGS).status();
    if let Err(e) = cmd_result {
        let e = Error::new(ErrorKind::Other, format!("{:?}", e));
        service_result = Err(WindowsServiceError::Winapi(e));
    }

    // For good measure.
    sleep(Duration::from_secs(3));

    // Report service as stopped.
    status_handle.set_service_status(ServiceStatus {
        service_type: ServiceType::OWN_PROCESS,
        current_state: ServiceState::Stopped,
        controls_accepted: ServiceControlAccept::empty(),
        exit_code: ServiceExitCode::Win32(0), // ignored in favor of `ServiceControlHandlerResult`
        checkpoint: 0,
        wait_hint: Duration::default(),
        process_id: None,
    })?;

    service_result
}
